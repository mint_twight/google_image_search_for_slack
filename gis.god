WORKING_DIR = File.expand_path(File.dirname(__FILE__))

God.watch do |w|
  w.name     = 'gis'
  w.dir      = WORKING_DIR
  w.start    = "ruby #{WORKING_DIR}/slack_client.rb"
  w.restart  = "kill `cat gis_pid`"
  w.log      = "#{WORKING_DIR}/god.log"
  w.interval = 5.seconds

  w.start_if do |start|
    start.condition(:process_running) do |c|
      c.running = false
    end
  end
end
