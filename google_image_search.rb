class GoogleImageSearch
  GOOGLE_SEARCH_URL = 'https://www.google.co.jp/search?tbm=isch&q='

  require 'open-uri'
  require 'kconv'
  require 'nokogiri'

  def initialize(keyword, position= 1)
    @url_keyword = keyword.join('+')
    @alt_keyword = keyword.join(' ')
    @position = (position - 1)
  end

  def search
    url = GOOGLE_SEARCH_URL + URI.encode(@url_keyword)
    html = open(url, "r:binary", "User-Agent" => "User-Agent: Mozilla/5.0").read
    html = Nokogiri::HTML(html.toutf8, nil, 'utf-8')
    images = html.css('img').select{ |img| img.attributes['alt'].value.include?(@alt_keyword) }
    images[@position].is_a?(Nokogiri::XML::Element) ? images[@position].attributes['src'].value : 'no image.'
  end
end
