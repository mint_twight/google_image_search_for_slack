require 'capybara'
require 'capybara/dsl'
require 'capybara/poltergeist'

class GoogleImageSearchJs
  include Capybara::DSL

  GOOGLE_SEARCH_URL = 'https://www.google.co.jp/search?tbm=isch&q='

  require 'open-uri'
  require 'nokogiri'
  require 'pry'

  def initialize(keyword, options={})
    Capybara.register_driver :poltergeist do |app|
      Capybara::Poltergeist::Driver.new(app, {:js_errors => false, :timeout => 1000 })
    end

    Capybara.default_driver = :poltergeist
    Capybara.javascript_driver = :poltergeist

    @session = Capybara::Session.new(:poltergeist)
    @session.driver.headers = { "User-Agent" => "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.116 Safari/537.36" }
    keyword.concat(options[:except]) unless options[:except].nil?
    @keyword = keyword.is_a?(Array) ? keyword.join(?+) : keyword
    @position = options[:position].to_i == 0 ? 0 : (options[:position].to_i - 1)
  end

  def search
    visit(@session)
    image_url = image_url_parse(Nokogiri::HTML.parse(@session.html))
    # @session.save_screenshot(Time.now.strftime("%Y%m%d_%Hh_%Mm.png"), :full => true)          # SS
    # File.open(Time.now.strftime("%Y%m%d_%Hh_%Mm.html"),"w"){ |file| file.puts @session.html } # HTML

    while image_url == '#'
      visit(@session)
      image_url = image_url_parse(Nokogiri::HTML.parse(@session.html))
    end

    @session.driver.quit

    # image_url = "HTTP Error: #{image_url}" unless alive?(image_url)
    image_url
  end

  private

  def visit(session)
    session.visit(GOOGLE_SEARCH_URL + URI.encode(@keyword))
  end

  def image_url_parse(html)
    a_tags = html.css('#rg_s a')
    if a_tags[@position].is_a?(Nokogiri::XML::Element)
      a_tag = a_tags[@position]
      image_url = URI.decode(a_tag.attributes['href'].value).split('&')[0]
      image_url.slice!('/imgres?imgurl=')
      image_url
    else
      'no image.'
    end
  end

  def alive?(image_url)
    require 'net/http'
    Net::HTTP.get_response(URI.parse(image_url)).is_a?(Net::HTTPOK)
  end
end
