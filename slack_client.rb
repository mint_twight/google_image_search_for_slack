require 'slack'
require_relative 'google_image_search_js'
require_relative 'slack_methods'
require 'pry'
require 'dotenv'

Dotenv.load
Slack.configure {|config| config.token = ENV['API_TOKEN'] }
client = Slack.realtime
setup

client.on :hello { puts 'Successfully connected.' }
client.on :message do |data|
  begin
    next unless data['text'] && data['text'].start_with?("<@#{@bot_id}>")
    parsed_text = option_parser(data['text'])
    (post(data['channel'], usage) and next) if parsed_text[:help_message] || parsed_text[:keyword].empty?
    parsed_text[:position].each do |position|
      params = parsed_text.merge(position: position)
      if parsed_text[:multi]
        parsed_text[:keyword].each do |keyword|
          post(data['channel'], GoogleImageSearchJs.new([keyword], params).search)
        end
      else
        post(data['channel'], GoogleImageSearchJs.new(params[:keyword], params).search)
      end
    end
  rescue Exception => e
    post(data['channel'], usage)
    puts e
    puts e.backtrace
  end
end

client.start
