require 'optparse'

def post_params(channel, text)
  {
    token: ENV['API_TOKEN'],
    channel: channel,
    text: text,
    as_user: true
  }
end

def post(channel, text)
  Slack.chat_postMessage post_params(channel, text)
end

def setup
  @bot_id ||= Slack.users_list['members']&.select{ |user| user['name'] == ENV['SLACK_BOT_NAME'] }[0]['id']
  `echo #{Process.pid} > gis_pid`
end

def option_parser(text)
  text = text.split[1..-1]
  opts = {:position => [0]}
  opt = OptionParser.new do |opt|
    opt.on('-p', '--position Number', Array, 'Change scraping position.') do |num|
      opts[:position] = num.map(&:to_i)
    end
    opt.on('-e', '--except Words', Array, 'Search excpting input words.') do |words|
      opts[:except] = words.map{ |word| word.insert(0, '-') }
    end
    opt.on('-m', '--multi', 'Multi aearch.') do |bool|
      opts[:multi] = bool
    end
    opt.on_tail("-h", "--help", "Show this message") do |bool|
      opts[:help_message] = bool
    end
  end
  opts[:keyword] = opt.parse!(text)
  return opts
end

def usage
  <<~'EOS'
    ```
    Usage: @gis_bot keyword [options]
      -p, --position Number            Change scraping position.
      -e, --except   Words             Search excpting input words.
      -m, --multi                      Multi search.
      -h, --help                       Show this message.
    ```
  EOS
end
